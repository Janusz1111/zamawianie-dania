package model;

enum Kuchnia {

    Pl("polska"),

    Mx("meksykańska"),

    It("włoska");



    public String jakaKuchnia;



    Kuchnia(String k) {

        jakaKuchnia = k;

    }



    String pokazKuchnie() {

        return jakaKuchnia;

    }

}