package model;


public enum MenuMx {



    Quesadilla( 8.6f),

    Tortilla(8.5f),

    Burrito(5.5f);



    public float cena;

    MenuMx(float c) {

        cena = c;

    }



    float pokazCene() {

        return cena;

    }

}