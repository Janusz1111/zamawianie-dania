package model;


public enum MenuPl {



    Bigos( 8.6f),

    Zalewajka(8.5f),

    Barszcz( 7.70f),

    Jajecznica(5.5f);



    public float cena;

    MenuPl(float c) {

        cena = c;

    }



    float pokazCene() {

        return cena;

    }

}